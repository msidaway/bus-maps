// How long to wait before changing the map.
var animateTime = 1000; // ms

// How long to wait before polling the TfL server for more data.
var updateTime = 30000; // ms

// How long should buses appear to "dwell" at a stop?
// Note: ideally, this should be >= updateTime to allow real-time information
// to influence the apparent dwell time.
var maxDwellTime = 32000; // ms

// The (ideal) minimum hop time between stops
var minHopTime = 32000; // ms

var cb_onFirstUpdate = [];
var doneFirstUpdate = false;

var map = null;
var bounds = null;
var iid = null;
var routeDetail = {};
var rtimes = {};
var posInfo = {};
var markers = {};
var animated = {};
var adjustment = 0;
var lastUpdate = 0;
var lastFileNow = 0;

function callCallbackList(cbs, data) {
	for (var i in cbs) {
		cbs[i](data);
	}
}

function makeBusIcon(lineName, zoomLevel) {
	zoomLevel = Math.max(10, Math.min(16, zoomLevel));
	var factor = Math.pow(2, 16 - zoomLevel);
	return L.divIcon({
		className: "bus-div-icon-" + zoomLevel,
		iconSize: L.point(50 / factor, 15 / factor),
		html: lineName,
	});
}

function animate() {
	var nowDate = Date.now();
	var now = nowDate + adjustment;

	if (nowDate - lastUpdate >= updateTime) {
		update();
	}

	for (var reg in rtimes) {
		animate_one(reg, rtimes[reg], markers[reg], now);
	}

	addEligibleBuses();

	//logPosInfo();
}

function logPosInfo() {
	var str = "";
	for (var reg in posInfo) {
		if (str.length > 0) {
			str += ", ";
		}
		str += reg + ": " + posInfo[reg].line + "-" + posInfo[reg].dir + "-" + posInfo[reg].routeFrac.toFixed(5);
		if (reg in rtimes) {
			str += " (";
			var timeStr = "";
			for (var i in rtimes[reg]) {
				if (timeStr.length > 0) {
					timeStr += ", ";
				}
				var num = Math.round((rtimes[reg][i].EstimatedTime - lastFileNow) / 1000);
				timeStr += num;
				if (num > 0) {
					break;
				}
			}
			str += timeStr;
			str += ")";
		}
	}
	if (str.length > 0) {
		str = Math.round((Date.now() - lastFileNow) / 1000) + ": " + str;
		console.log(str);
	}
}

function animate_one(reg, times, marker, now) {
	if (times.length == 0) {
		return; // Wait for more data.
	}
	// Find the first bus-stop (stop i) that has not yet
	// been reached by the bus.
	var i;
	for (i = 0; i < times.length; i++) {
		if (now < times[i].EstimatedTime) {
			break;
		}
	}
	var j, frac;
	// Deal with edge cases.
	if (i == 0) {
		// At start of available data
		j = 1;
		frac = 0.0;

		// Try to interpolate an earlier time so that position
		// interpolation works.

		if (times.length > 1) {
			interpolatePreviousTime(times, now);
		}

	} else if (i == times.length) {
		// At end of available data
		if (times.length == 1) {
			i = 0;
			j = 1;
			frac = 0.0;
		} else {
			i = times.length - 2;
			j = times.length - 1;
			frac = 1.0;
		}
	} else {
		// Change variables so that the bus will now be between
		// stop i and stop j.
		j = i;
		i--;
		// How far along is the bus, on a scale of 0 to 1,
		// from stop i to stop j?
		var hopTime = times[j].EstimatedTime - times[i].EstimatedTime;
		var outTime = now - times[i].EstimatedTime;
		if (hopTime > minHopTime) {
			var dwellTime = Math.min(maxDwellTime, hopTime - minHopTime);
			hopTime -= dwellTime;
			outTime -= dwellTime;
			if (outTime < 0) {
				outTime = 0;
			}
		}
		frac = outTime / hopTime;
	}
	var irr = interpolateRoute(times[i].StopID,
							   j < times.length ? times[j].StopID : null,
							   times[i].LineName, times[i].DirectionID, frac);
	if (!irr) {
		return;
	}
	if (posInfo[reg] &&
		posInfo[reg].line == times[i].LineName &&
		posInfo[reg].dir == times[i].DirectionID &&
		posInfo[reg].routeFrac > irr.routeFrac) {

		// Prevent marker from going backwards.
		return;
	}
	posInfo[reg] = {
		"line": times[i].LineName,
		"dir": times[i].DirectionID,
		"routeFrac": irr.routeFrac,
	};
	if (irr.pos != null) {
		animated[reg] = true;
		marker.setAngle(-irr.angle);
		marker.setLatLng(irr.pos);
	}
}

function interpolateRoute(id1, id2, line, dir, frac) {
	if (!routeDetail || !routeDetail[line] || !routeDetail[line][dir]) {
		return null;
	}
	var rd = routeDetail[line][dir];
	var si1 = rd.stop_ids.indexOf(id1);

	// Ensure that we have a second index.
	var si2;
	if (id2) {
		si2 = rd.stop_ids.indexOf(id2);
	} else if (frac == 0.0) {
		si2 = si1 + 1;
	} else {
		return null;
	}

	// Ensure that the second index is different from the first
	if (si1 == si2) {
		frac = 0.0;
		si2++;
	}

	// Ensure that .indexOf() returned correct values, if applicable.
	if (si1 == -1 || si2 == -1 || si1 > si2) {
		return null;
	}

	// Ensure that both indices are valid.
	if (si2 == rd.stop_ids.length && frac == 0.0) {
		if (si2 - 1 > si1) {
			si2--;
		} else if (si1 > 0) {
			si2 = si1;
			si1--;
			frac = 1.0;
		}
	}
	if (si2 >= rd.stop_ids.length) {
		return null;
	}

	var sr = cumulSearch(rd.dists, rd.stops[si1], rd.stops[si2], frac);
	var p1 = rd.points[sr.index];
	var p2 = rd.points[sr.index + 1];
	var stop1 = rd.stops[si1];
	var stop2 = rd.stops[si2];
	var routeFrac = (rd.dists[stop1] +
				     frac * (rd.dists[stop2] - rd.dists[stop1])) /
					rd.dists[rd.dists.length - 1];
	return {
		"pos": [p1[0] + (p2[0] - p1[0]) * sr.frac,
		        p1[1] + (p2[1] - p1[1]) * sr.frac],
		"angle": Math.atan2(p2[0] - p1[0],
							(p2[1] - p1[1])
								* Math.cos(p1[0] * Math.PI / 180.0))
					* 180.0 / Math.PI,
		"routeFrac": routeFrac,
	};
}

function interpolatePreviousTime(times, now) {
	var line = times[0].LineName;
	var dir = times[0].DirectionID;
	if (routeDetail && routeDetail[line] && routeDetail[line][dir]) {
		var rd = routeDetail[line][dir];
		var si1 = rd.stop_ids.indexOf(times[0].StopID);
		var si2 = rd.stop_ids.indexOf(times[1].StopID);
		if (si1 != -1 && si2 != -1 && si1 < si2 && 0 < si1) {
			var stop1 = rd.stops[si1];
			var stop2 = rd.stops[si2];
			var dist = rd.dists[stop2] - rd.dists[stop1];
			var time = times[1].EstimatedTime -
					   times[0].EstimatedTime;
			if (time > 0 && dist > 0) {
				var speed = dist / time;
				var si0;
				var stop0;
				var new_est;
				for (si0 = si1 - 1; si0 >= 0; si0--) {
					stop0 = rd.stops[si0];
					var new_dist = rd.dists[stop1] -
								   rd.dists[stop0];
					new_est = times[0].EstimatedTime -
							  new_dist / speed;
					if (now >= new_est) {
						break;
					}
				}
				if (si0 >= 0) {
					//console.log("Interpolated previous time (stop "+si0+", before "+si1+" from data)");
					times.splice(0, 0, {
						StopID:             rd.stop_ids[si0],
						Latitude:           rd.points[stop0][0],
						Longitude:          rd.points[stop0][1],
						LineName:           line,
						DirectionID:        dir,
						RegistrationNumber: times[0].RegistrationNumber,
						EstimatedTime:      new_est,
					});
				}
			}
		}
	}
}

function cumulSearch(arr, i1, i2, frac) {
	var dist = frac * (arr[i2] - arr[i1]);
	while (i2 > i1 + 1) {
		var ic = Math.floor((i1 + i2) / 2);
		if (dist < arr[ic] - arr[i1]) {
			i2 = ic;
		} else {
			dist -= arr[ic] - arr[i1];
			i1 = ic;
		}
	}
	return { "index": i1, "frac": dist / (arr[i2] - arr[i1]) };
}

function removeBus(reg) {
	map.removeLayer(markers[reg]);
	delete markers[reg]; // is this safe inside for...in ?
	delete posInfo[reg];
	delete rtimes[reg]; // Prevent it from coming back!
	delete animated[reg];
}

function deleteRoute(route) {
	routeDetail[route] = false; // Prevent it from coming back (ever)
	for (var reg in markers) {
		if (rtimes[reg].length > 0 && rtimes[reg][0].LineName == route) {
			removeBus(reg);
		}
	}
}

function updateIcon(reg) {
	if (reg in markers && reg in rtimes && rtimes[reg].length > 0) {
		var line = rtimes[reg][0].LineName;
		var popupOpen = map.hasLayer(markers[reg]._popup);
		if (popupOpen) {
			markers[reg].closePopup();
		}
		markers[reg].setIcon(makeBusIcon(line, map.getZoom()));
		markers[reg].bindPopup(line + "<br>" + reg);
		if (popupOpen) {
			markers[reg].openPopup();
		}
	}
}

function update() {

	lastUpdate = Date.now();

	get_data(map.getCenter(), function(regs, now) {

		lastFileNow = now;

		// Remove bus markers not present in new data.
		// N.B. bus may already have been removed from markers by animate_one()
		for (var reg in markers) {
			if (!(reg in regs)) {
				removeBus(reg);
			}
		}

		var newLineNames = {};

		// Add/update stored data from new data (does
		// not move or create markers).
		for (var reg in regs) {
			if (regs[reg].length > 0 && routeDetail[regs[reg][0].LineName] === false) {
				// line deleted as no route detail available
				continue;
			}
			if (!(reg in rtimes) || (regs[reg].length > 0 && regs[reg][0].EstimatedTime <= now)) {
				rtimes[reg] = regs[reg];
			} else {
				// N.B. there are probably also cases where we want to keep a prediction
				// that is still in the future, but has been removed from the new data set
				// (but these are harder to determine) (also, this will only happen at the
				// beginning - although it may drag on if it happens repeatedly to the same
				// bus).
				var first;
				for (first = 0; first < rtimes[reg].length; first++) {
					if (rtimes[reg][first].EstimatedTime > now) {
						break;
					}
				}
				var lastKnown = null;
				if (first > 0) {
					// Keep last known time/position...
					lastKnown = rtimes[reg][first-1];

					// ...but not if the route has changed.
					if (regs[reg].length > 0 &&
						(lastKnown.LineName != regs[reg][0].LineName ||
						 lastKnown.DirectionID != regs[reg][0].DirectionID)) {
						lastKnown = null;
					}
				}
				if (lastKnown) {
					rtimes[reg] = [ lastKnown ].concat(regs[reg])
				} else {
					rtimes[reg] = regs[reg];
				}
			}

			if (regs[reg].length > 0) {
				newLineNames[regs[reg][0].LineName] = 1;
			}
		}

		for (var lineName in newLineNames) {
			if (!(lineName in routeDetail) && routeDetail[lineName] !== false) {
				loadRouteDetail(lineName);
			}
		}

		var nowDate = Date.now();
		adjustment = now - nowDate;

		// Add new markers to map.
		for (var reg in rtimes) {
			var times = rtimes[reg];

			if (!(reg in markers)) {
				markers[reg] = L.marker([times[0].Latitude, times[0].Longitude]);
				updateIcon(reg);
			}
			
			//markers[reg].setContent(times[0].LineName+":"+reg);
			//markers[reg].setContent(reg + "<br>" + times[0].EstimatedTime/1000 +
			//						"<br>" + times[1].EstimatedTime/1000);
		}

		if (!doneFirstUpdate) {
			// Call onFirstUpdate callback(s) if provided. This
			// could, e.g., start updating the marker positions.
			callCallbackList(cb_onFirstUpdate);
			doneFirstUpdate = true;
		}

	});
}

// adjusted for a roughly spherical Earth
function adjustedNorm(v1, v2) {
	var latDist = v2[0] - v1[0];
	var lonDist = v2[1] - v1[1];
	var meanLat = (v1[0] + v2[0]) / 2.0;
	var scaling = Math.cos(meanLat / 180.0 * Math.PI);
	lonDist *= scaling;
	return Math.sqrt(latDist*latDist + lonDist*lonDist);
}

function onNewRouteDetail(data, textStatus, jqXHR, expRoute) {
	for (var route in data) {
		routeDetail[route] = data[route];
		for (var dir in data[route]) {
			var points = data[route][dir]["points"];
			var stops = data[route][dir]["stops"];
			var p = L.polyline(points,
					   { color: 'blue' }).addTo(map);
			if (bounds == null) {
				bounds = p.getBounds();
			} else {
				bounds.extend(p.getBounds());
			}
			//map.fitBounds(bounds);

			// Precompute cumulative relative traversed
			// distances to reach every point from the
			// starting bus-stop.
			data[route][dir]["dists"] =
				new Array(points.length);
			data[route][dir]["dists"][0] = 0;
			for (var i = 0, j = 1; i < points.length - 1;
				 i++, j++) {
				data[route][dir]["dists"][j] =
					data[route][dir]["dists"][i] +
					adjustedNorm(points[i], points[j]);
			}
		}
	}
	addEligibleBuses();
	if (!(expRoute in data) && !(expRoute in routeDetail)) {
		deleteRoute(expRoute);
	}
}

function addEligibleBuses() {
	for (var reg in rtimes) {
		if (rtimes[reg].length <= 0) {
			continue;
		}
		var line = rtimes[reg][0].LineName;
		if (line in routeDetail &&
			routeDetail[line] !== false &&
			rtimes[reg][0].DirectionID in routeDetail[line] &&
			animated[reg] &&
			reg in markers &&
			!map.hasLayer(markers[reg])) {

			markers[reg].addTo(map);
		}
	}
}

function loadRouteDetail(route) {
	if (route in routeDetail) {
		return;
	}

	if (!route.match(/^[A-Za-z0-9_-]+$/)) {
		console.log("Bad route name: " + route);
		return;
	}

	$.ajax({ url: detailURLTmpl.replace('{0}', route),
			 success: function(a, b, c) { return onNewRouteDetail(a, b, c, route); },
			 error: function(req, stat, thrown) { if (req.status == 404) {
				deleteRoute(route);
			 } },
			 dataType: 'json',
			});
}

function addOnFirstUpdateCallback(cb) {
	cb_onFirstUpdate.push(cb);
}

function setLatLon(latlon) {
	if (map) {
		map.setCenter(latlon);
	} else {
		initLatLon = latlon;
	}
}

function setZoom(zoom) {
	if (map) {
		map.setZoom(zoom);
	} else {
		initZoom = zoom;
	}
}

function getLatLon() { return map && map.getCenter(); }
function getZoom() { return map && map.getZoom(); }

// on page structure loaded
function startBusMap(id) {
	map = L.map(id).setView(initLatLon, initZoom);

	L.tileLayer(tileServerURL, tileServerSettings).addTo(map);

	addOnFirstUpdateCallback(function () {
		animate();
		iid = window.setInterval(animate, animateTime);
	});

	update();

	map.on("moveend", function() {
		update();
	});

	map.on("zoomend", function() {
		for (var reg in markers) {
			updateIcon(reg);
		}
	});
}
