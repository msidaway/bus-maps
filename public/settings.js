var initLatLon = [51.5073, -0.12755];
var initZoom = 15;

var tileServerURL = "http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png";

// MapQuestOSM
//var tileServerURL = "http://otile{s}.mqcdn.com/tiles/1.0.0/map/{z}/{x}/{y}.png";

var tileServerSettings = {
	maxZoom: 18,
	attribution: "&copy; OpenStreetMap",

// MapQuestOSM
//	attribution: "&copy; OpenStreetMap; tiles courtesy of <a href=\"http://www.mapquest.com/\" target=\"_blank\">MapQuest</a> <img src=\"http://developer.mapquest.com/content/osm/mq_logo.png\">",
//	subdomains: "1234",

};

// From origin web server
var tflInstantBusAPI = "/tfl_countdown_instant_V1";

// Direct from TfL
//var tflInstantBusAPI = "http://countdown.api.tfl.gov.uk/interfaces/ura/instant_V1";

var detailURLTmpl = "/gen/detail/{0}.json";
