var currentReq = null;

function objToString(obj) {
	var str = "{";
	for (var key in obj) {
		if (str.length > 1) {
			str += ",";
		}
		str += key + ":" + obj[key];
	}
	str += "}";
	return str;
}

function read_tfl_json(raw) {
	return JSON.parse('[' + $.trim(raw).split(/[\r\n]+/).join(',') + ']');
}

function get_data(centre, cb) {
	var fields = ["LineName"];
	if (currentReq) {
		currentReq.abort();
	}
	currentReq = $.get(tflInstantBusAPI + "?ReturnList="+fields.join(",")+
			"&Circle="+centre.lat+","+centre.lng+",2000",
		  function(raw) {
		var json = read_tfl_json(raw);
		var routes = {};
		for (var i = 1; i < json.length; i++) {
			routes[json[i][1]] = 1;
		}
		get_data_impl(Object.keys(routes), cb);
	}, 'text');
}

function get_data_impl(routes, cb) {

	// Remove explicitly deleted routes
	if (routeDetail) {
		for (var route in routes) {
			// If explicitly deleted
			if (routeDetail[route] === false) {
				routes.splice(routes.indexOf(route), 1);
			}
		}
	}

	//console.log("Routes: " + routes.join(", "));

	// this is the ordering (of columns) always returned by the API - but it also adds an extra column 0
	// (for some reason I do not understand, LineName and RegistrationNumber are returned in the
	// opposite order from that given in the API spec.)
	var fields = ["StopID","Latitude","Longitude","LineName","DirectionID","RegistrationNumber","EstimatedTime"];
	currentReq = $.get(tflInstantBusAPI + "?ReturnList="+fields.join(",")+
			"&LineName=" + routes.join(","),
		  function(raw) {
		var json = read_tfl_json(raw);
		currentReq = null;
		var regs = {};
		// N.B. element 0 is type/version/timestamp
		var now = json[0][2];
		for (var i = 1; i < json.length; i++) {

			// Create object using field names
			var obj = {};
			for (var fi = 0; fi < fields.length; fi++) {
				obj[fields[fi]] = json[i][fi+1];
			}

			//window.alert(objToString(obj));

			if (!(obj.RegistrationNumber in regs)) {
				regs[obj.RegistrationNumber] = [];
			}
			regs[obj.RegistrationNumber].push(obj);
		}
		for (var reg in regs) {
			regs[reg].sort(function(a, b) { return a.EstimatedTime < b.EstimatedTime ? -1 : a.EstimatedTime == b.EstimatedTime ? 0 : 1; });
		}
		cb(regs, now);
	}, 'text');
}
