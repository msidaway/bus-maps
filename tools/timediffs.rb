#!/usr/bin/env ruby

lines = File.readlines(ARGV[0])

lines.map! {|line| line.strip.gsub(/"/, '').split(',') }

puts "    #{lines[0][1]}"

(1...lines.size).each{|j|
	i = j - 1
	printf "%3u %s\n", lines[j][-1][0..-4].to_i - lines[i][-1][0..-4].to_i, lines[j][1]
}
