#!/usr/bin/env ruby

lines = File.readlines(ARGV[0])

lines.map! {|line| line.strip.gsub(/"/, '').split(",") }

non_num = (0...lines[0].size).map {|i|
	lines.any? {|line| line[i] !~ /^-?[0-9]*\.?[0-9]*$/ || line[i] !~ /[0-9]/ }
}

lines = lines.transpose.each.each_with_index.map {|column, i|
	if non_num[i]
		column.map {|item| (item[0] == '"' && item[-1] == '"') ? item : ('"' + item + '"') }
	else
		column
	end
}.transpose.map {|line| "[" + line.join(',') + "]" }.join(",\n")

varname = ARGV[0].sub(/\..*$/, '')

puts "var #{varname} = [#{lines}];"
