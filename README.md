How to run
----------

* Run "mkdir public/gen".
* Run my osm-data project, then copy osm-data/out/detail into bus-maps/public/gen.
* Make sure Ruby and Sinatra are installed.
* Run "ruby sinatra.rb".
* Visit http://localhost:8000/ in your browser.
