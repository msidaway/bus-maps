#!/usr/bin/env ruby

require 'sinatra'
require 'net/http'

set :bind, "127.0.0.1"
set :port, 8000

get '/' do
	send_file 'public/index.html'
end

get '/tfl_countdown_instant_V1' do
	Net::HTTP.get(URI("http://countdown.api.tfl.gov.uk/interfaces/ura/instant_V1?" +
						request.query_string))
end
